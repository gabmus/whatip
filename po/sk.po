# Slovak translations for whatip package.
# Copyright (C) 2020 THE whatip'S COPYRIGHT HOLDER
# This file is distributed under the same license as the whatip package.
# martin-sk <https://github.com/martin-sk>, 2020.
#
msgid ""
msgstr ""
"Project-Id-Version: whatip 0.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2020-10-25 08:45+0100\n"
"PO-Revision-Date: 2020-10-25 08:50+0100\n"
"Last-Translator: martin-sk <https://github.com/martin-sk>\n"
"Language-Team: martin-sk <https://github.com/martin-sk>\n"
"Language: sk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Poedit 2.4.1\n"

#: ../whatip/ping.py:55
msgid "Error connecting to avahi daemon. Hostname resolution disabled."
msgstr "Chyba pri pripájaní sa k démonovi avahi. Rozlíšenie názvu hostiteľa je zakázané."

#: ../whatip/ping.py:70
msgid "Unknown hostname"
msgstr "Neznámy názov hostiteľa"

#: ../whatip/network_interface.py:61
#: ../whatip/network_interface_listbox_row.py:19
msgid "Public Internet"
msgstr "Verejný internet"

#: ../whatip/network_interface.py:64
msgid "Unknown city"
msgstr "Neznáme mesto"

#: ../whatip/network_interface.py:72
msgid "Unknown country"
msgstr "Neznáma krajina"

#: ../whatip/port_listbox_row.py:72 ../whatip/port_listbox_row.py:73
msgid "Unknown service"
msgstr "Neznáma služba"

#: ../whatip/port_listbox_row.py:95
msgid "Error parsing json"
msgstr "Chyba pri analýze JSON"

#: ../whatip/port_listbox_row.py:97
msgid "Retrying..."
msgstr "Opakuje sa pokus ..."

#: ../whatip/port_listbox_row.py:115
msgid "Port reachable"
msgstr "Port dostupný"

#: ../whatip/port_listbox_row.py:116
msgid "Port unreachable"
msgstr "Port nedostupný"

#: ../whatip/lan_listbox.py:20
msgid "Scanning LAN..."
msgstr "Skenovanie LAN..."

#: ../whatip/network_interface_listbox.py:18
msgid "No network interfaces connected"
msgstr "Nie sú pripojené žiadne sieťové rozhrania"

#: ../whatip/__main__.py:110
msgid ""
"What IP connects to third party services to retrieve geolocation information, "
"your public IP and determine if you are online.\n"
"\n"
"Do you want to keep this feature enabled?\n"
"You can always change this setting in the application preferences."
msgstr ""
"Aká IP je pripojená k službám tretích strán na získanie informácií o geolokácii, "
"vaša verejná IP a určenie, či ste online.\n"
"\n"
"Chcete ponechať túto funkciu povolenú?\n"
"Toto nastavenie môžete kedykoľvek zmeniť v nastaveniach aplikácie."

#: ../whatip/port_listbox.py:12
msgid "No ports detected"
msgstr "Nenájdené žiadne porty"

#: ../whatip/main_stack.py:23
msgid "IP"
msgstr "IP"

#: ../whatip/main_stack.py:31
msgid "Ports"
msgstr "Porty"

#: ../whatip/main_stack.py:39
msgid "LAN"
msgstr "LAN"

#: ../whatip/settings_window.py:132
msgid "General"
msgstr "Všeobecné"

#: ../whatip/settings_window.py:136
msgid "General Settings"
msgstr "Všeobecné nastavenia"

#: ../whatip/settings_window.py:139
msgid "Enable third party services"
msgstr "Zapnúť služby tretích strán"

#: ../whatip/network_interface_listbox_row.py:20
msgid "Wired"
msgstr "Pripojené"

#: ../whatip/network_interface_listbox_row.py:21
msgid "Wireless"
msgstr "Bezdrôtové"

#: ../whatip/network_interface_listbox_row.py:22
msgid "Mobile"
msgstr "Mobilné"

#: ../whatip/network_interface_listbox_row.py:23
msgid "Virtual bridge"
msgstr "Virtuálny most"

#: ../whatip/network_interface_listbox_row.py:24
msgid "Tunnel"
msgstr "Tunel"

#: ../whatip/network_interface_listbox_row.py:25
msgid "Loopback"
msgstr "Slučka"

#: ../whatip/network_interface_listbox_row.py:26
msgid "WireGuard"
msgstr "WireGuard"

#: ../whatip/network_interface_listbox_row.py:27
msgid "Unknown"
msgstr "Neznáme"

#: ../whatip/network_interface_listbox_row.py:77
#, python-brace-format
msgid "{0}, {1} - {2}"
msgstr "{0}, {1} - {2}"

#: ../whatip/network_interface_listbox_row.py:83
#, python-brace-format
msgid "{0} - {1}"
msgstr "{0} - {1}"

#: ../data/ui/menu.xml:6
msgid "Preferences"
msgstr "Nastavenia"

#: ../data/ui/menu.xml:10
msgid "Keyboard Shortcuts"
msgstr "Klávesové skratky"

#: ../data/ui/menu.xml:14
msgid "About What IP"
msgstr "O aplikácií What IP"

#: ../data/ui/shortcutsWindow.xml:13
msgid "Open Keyboard Shortcuts"
msgstr "Otvoriť klávesové skratky"

#: ../data/ui/shortcutsWindow.xml:19
msgid "Open Menu"
msgstr "Otvoriť Menu"

#: ../data/ui/shortcutsWindow.xml:31
msgid "Quit"
msgstr "Ukončiť"

#: ../data/ui/net_iface_listbox_row.glade:68
#: ../data/ui/lan_listbox_row.glade:156
msgid "Address"
msgstr "Adresa"

#: ../data/ui/net_iface_listbox_row.glade:87
#: ../data/ui/net_iface_listbox_row.glade:164
#: ../data/ui/lan_listbox_row.glade:63
msgid "Copy"
msgstr "Kopírovať"

#: ../data/ui/net_iface_listbox_row.glade:126
msgid "Location"
msgstr "Umiestnenie"

#: ../data/ui/net_iface_listbox_row.glade:147
msgid "IPv6 address"
msgstr "IPv6 adresa"

#: ../data/ui/net_port_listbox_row.glade:46
msgid "TCP"
msgstr "TCP"

#: ../data/ui/net_port_listbox_row.glade:65
msgid "UDP"
msgstr "UDP"

#: ../data/ui/net_port_listbox_row.glade:117
msgid "Listening on"
msgstr "Počúvam na"

#: ../data/ui/net_port_listbox_row.glade:176
msgid "Test connection"
msgstr "Test pripojenia"

#: ../data/ui/headerbar.glade:13
msgid "Refresh"
msgstr "Obnoviť"

#: ../data/ui/headerbar.glade:48
msgid "Menu"
msgstr "Menu"

#: ../data/ui/lan_listbox_row.glade:43
msgid "__hostname__"
msgstr "__hostname__"

#: ../data/ui/lan_listbox_row.glade:90
msgid "Go to webpage"
msgstr "Navštív webstránku"

#: ../data/ui/lan_listbox_row.glade:117
msgid "Open shared folder"
msgstr "Otvoriť zdieľaný priečinok"

#: ../data/org.gabmus.whatip.desktop.in:3
msgid "@prettyname@"
msgstr "@prettyname@"

#: ../data/org.gabmus.whatip.desktop.in:4
msgid "Info on your IP"
msgstr "Info o vašej IP"

#: ../data/org.gabmus.whatip.desktop.in:12
msgid "network;ip;address;port;"
msgstr "sieť;ip;adresa;port;"

#: ../data/org.gabmus.whatip.appdata.xml.in:4
msgid "What IP"
msgstr "What IP"

#: ../data/org.gabmus.whatip.appdata.xml.in:5
msgid "Gabriele Musco"
msgstr "Gabriele Musco"

#: ../data/org.gabmus.whatip.appdata.xml.in:15
msgid ""
"🌐️ Get your IP easily: Be it local, public or a virtual interface's, it's easy "
"to understand and one click away"
msgstr ""
"🌐️ Získajte svoju IP adresu ľahko: či už je to miestna, verejná alebo virtuálna, "
"je ľahká na používanie a stačí jedno kliknutie"

#: ../data/org.gabmus.whatip.appdata.xml.in:16
msgid ""
"🔒️ Make sure your VPN is working: What IP shows your location based on your IP "
"address, so that you can make sure your VPN is working"
msgstr ""
"🔒️ Uistite sa, že vaša VPN funguje: Aká IP zobrazuje vašu polohu na základe vašej "
"adresy IP, aby ste sa mohli ubezpečiť, že vaša VPN funguje"

#: ../data/org.gabmus.whatip.appdata.xml.in:17
msgid ""
"🧪 Test your ports: List the ports listening on your system, and check if "
"they're publicly reachable"
msgstr ""
"🧪 Vyskúšajte svoje porty: Uveďte zoznamy portov počúvajúcich vo vašom "
"systéme a skontrolujte, či sú verejne dostupné"

#: ../data/org.gabmus.whatip.appdata.xml.in:18
msgid ""
"🖧 Discover devices on your LAN: List all devices on your LAN and easily copy "
"their addresses"
msgstr ""
"🖧 Objavte zariadenia vo vašej sieti LAN: Uveďte zoznam všetkých zariadení "
"vo svojej sieti LAN a jednoducho kopírujte ich adresy"

#: ../data/org.gabmus.whatip.appdata.xml.in:39
msgid "Upgraded to libhandy 1"
msgstr "Aktualizované na libhandy 1"

#: ../data/org.gabmus.whatip.appdata.xml.in:40
msgid "Added hostname resolution for devices in the LAN"
msgstr "Pridané rozlíšenie názvu hostiteľa pre zariadenia v sieti LAN"

#: ../data/org.gabmus.whatip.appdata.xml.in:41
msgid "Detect HTTP and FTP protocols for devices in the LAN"
msgstr "Zistiť protokoly HTTP a FTP pre zariadenia v sieti LAN"

#: ../data/org.gabmus.whatip.appdata.xml.in:48
msgid "Various bug fixes"
msgstr "Rôzne opravy chýb"

#: ../data/org.gabmus.whatip.appdata.xml.in:49
msgid "UI improvements"
msgstr "Vylepšenia používateľského rozhrania"

#: ../data/org.gabmus.whatip.appdata.xml.in:56
msgid "Minor bug fixes"
msgstr "Drobné opravy chýb"

#: ../data/org.gabmus.whatip.appdata.xml.in:63
msgid "First release"
msgstr "Prvé vydanie"

#~ msgid "View"
#~ msgstr "Zobraziť"

#~ msgid "View Settings"
#~ msgstr "Nastavenia zobrazenia"

#~ msgid "Enable client side decoration"
#~ msgstr "Povoliť dekoráciu na strane klienta"

#~ msgid "Internet"
#~ msgstr "Internet"

#~ msgid "Service"
#~ msgstr "Služba"

#~ msgid "Unknown region"
#~ msgstr "Neznámy región"

#~ msgid "Service: "
#~ msgstr "Služba: "

#~ msgid "Listening on: "
#~ msgstr "Počúvam na: "

#~ msgid "Address: "
#~ msgstr "Adresa: "

#~ msgid "Location: "
#~ msgstr "Umiestnenie: "
