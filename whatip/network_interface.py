from enum import Enum
from threading import Thread
from gi.repository import GObject, GLib
import requests
import json
from gettext import gettext as _
from whatip.test_connection import is_online
from whatip.requests_headers import GET_HEADERS
from subprocess import check_output


class NetType(Enum):
    INTERNET = 'Internet'
    WIRELESS = 'Wireless'
    WIRED = 'Wired'
    MOBILE = 'Mobile'
    LOOPBACK = 'Loopback'
    VIRTUAL_BRIDGE = 'Virtual bridge'
    TUNNEL = 'Tunnel'
    WIREGUARD = 'WireGuard'
    DOCKER = 'Docker'
    UNKNOWN = 'Unknown'


PHYSICAL_INTERFACES = {NetType.WIRED, NetType.WIRELESS, NetType.MOBILE}


class NetworkInterface(GObject.Object):
    __address: str = ''
    __address_ipv6: str = ''
    __interface: str = ''
    __interface_type: NetType = NetType.UNKNOWN
    __geolocation: str = ''
    __mac_addr: str = ''
    __subnet_mask: int = 24
    __valid: bool = False

    @GObject.Property(type=str)
    def address(self) -> str:  # type: ignore
        return self.__address

    @address.setter
    def address(self, nval: str):  # type: ignore
        self.__address = nval

    @GObject.Property(type=str)
    def address_ipv6(self) -> str:  # type: ignore
        return self.__address_ipv6

    @address_ipv6.setter
    def address_ipv6(self, nval: str):  # type: ignore
        self.__address_ipv6 = nval

    @GObject.Property(type=str)
    def interface(self) -> str:  # type: ignore
        return self.__interface

    @interface.setter
    def interface(self, nval: str):  # type: ignore
        self.__interface = nval

    @GObject.Property
    def interface_type(self) -> NetType:  # type: ignore
        return self.__interface_type

    @interface_type.setter
    def interface_type(self, nval: NetType):  # type: ignore
        self.__interface_type = nval

    @GObject.Property(type=str)
    def geolocation(self) -> str:  # type: ignore
        return self.__geolocation

    @geolocation.setter
    def geolocation(self, nval: str):  # type: ignore
        self.__geolocation = nval

    @GObject.Property(type=str)
    def mac_addr(self) -> str:  # type: ignore
        return self.__mac_addr

    @mac_addr.setter
    def mac_addr(self, nval: str):  # type: ignore
        self.__mac_addr = nval

    @GObject.Property(type=int)
    def subnet_mask(self) -> int:  # type: ignore
        return self.__subnet_mask

    @subnet_mask.setter
    def subnet_mask(self, nval: int):  # type: ignore
        self.__subnet_mask = nval

    @GObject.Property(type=bool, default=False)
    def valid(self) -> bool:  # type: ignore
        return self.__valid

    @valid.setter
    def valid(self, nval: bool):  # type: ignore
        self.__valid = nval

    def is_physical(self) -> bool:
        return self.__interface_type in PHYSICAL_INTERFACES

    def has_lan(self) -> bool:
        return self.__interface_type in (NetType.WIRED, NetType.WIRELESS)

    def __init__(self, addr_dict, internet=False):
        super().__init__()
        if internet:
            is_online(self.get_internet_data)
        else:
            if len(addr_dict.get('addr_info', [])) > 0:
                inet_list = list(
                    filter(
                        lambda d: d['family'] == 'inet', addr_dict['addr_info']
                    )
                )
                inet = inet_list[0] if len(inet_list) > 0 else None
                inet6_list = list(
                    filter(
                        lambda d: d['family'] == 'inet6',
                        addr_dict['addr_info'],
                    )
                )
                inet6 = inet6_list[0] if len(inet6_list) > 0 else None
                if inet is not None:
                    self.address = inet['local']
                    self.subnet_mask = inet['prefixlen']
                if inet6 is not None:
                    self.address_ipv6 = inet6['local']
                self.interface = addr_dict['ifname']
                self.mac_addr = addr_dict.get('address', None)
                self.interface_type = self.guess_interface_type(self.interface)
            self.valid = (
                self.address is not None and self.interface is not None
            )

    def guess_interface_type(self, iname) -> NetType:
        return (
            (iname == 'lo' and NetType.LOOPBACK)
            or ('eth' in iname and NetType.WIRED)
            or ('en' in iname and NetType.WIRED)
            or ('em' in iname and NetType.WIRED)
            or ('wl' in iname and NetType.WIRELESS)
            or ('wwan' in iname and NetType.MOBILE)
            or ('virbr' in iname and NetType.VIRTUAL_BRIDGE)
            or ('tun' in iname and NetType.TUNNEL)
            or ('wg' in iname and NetType.WIREGUARD)
            or ('docker' in iname and NetType.DOCKER)
            or NetType.UNKNOWN
        )

    def get_internet_data(self, online: bool):
        if not online:
            return

        def cb(geo: dict):
            self.address = geo.get('ip', None)
            # NOTE: this order is important
            self.interface_type = NetType.INTERNET
            self.interface = _('Public Internet')
            geoloc_data = {
                'city': geo.get('city', _('Unknown city')),
                'region': geo.get('region_name', geo.get('region')),
                'country': geo.get(
                    'country_name',
                    geo.get('country', _('Unknown country')),
                ),
            }
            if geoloc_data['region'] is not None:
                self.geolocation = _('{0}, {1} - {2}').format(
                    geoloc_data['city'],
                    geoloc_data['region'],
                    geoloc_data['country'],
                )
            else:
                self.geolocation = _('{0} - {1}').format(
                    geoloc_data['city'], geoloc_data['country']
                )
            self.valid = (
                self.address is not None and self.interface is not None
            )

        def af():
            for retry in range(0, 10):
                res = requests.get(
                    'https://geoip.fedoraproject.org/city'
                    if retry < 5
                    else 'https://ifconfig.co/json',
                    timeout=10,
                    headers=GET_HEADERS,
                )
                if 200 <= res.status_code <= 299:
                    try:
                        geo = json.loads(res.text)
                    except Exception:
                        continue
                    GLib.idle_add(cb, geo)
                    return

        Thread(target=af, daemon=True).start()

    def __repr__(self):
        return f'NetworkInterface {self.interface}, address: {self.address}'

    @classmethod
    def get_all_interfaces(cls):
        out = json.loads(check_output('ip -j addr', shell=True))
        return [cls(adict) for adict in out]
