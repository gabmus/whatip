from typing import cast
from gi.repository import Gtk, Gio, GLib, GObject
from whatip.clipboard_helper import clipboard_copy
from whatip.lan_device import LanDevice


@Gtk.Template(resource_path='/org/gabmus/whatip/ui/lan_listbox_row.ui')
class LanListboxRow(Gtk.ListBoxRow):
    __gtype_name__ = 'LanListboxRow'
    mainbox = cast(Gtk.Box, Gtk.Template.Child())
    address_label = cast(Gtk.Label, Gtk.Template.Child())
    hostname_label = cast(Gtk.Label, Gtk.Template.Child())
    go_http_btn = cast(Gtk.Button, Gtk.Template.Child())
    go_ftp_btn = cast(Gtk.Button, Gtk.Template.Child())
    copy_btn = cast(Gtk.Button, Gtk.Template.Child())

    def __init__(self, lan_dev: LanDevice):
        super().__init__()
        self.lan_dev = lan_dev

        self.lan_dev.bind_property(
            'address',
            self.address_label,
            'label',
            GObject.BindingFlags.SYNC_CREATE,
        )
        self.lan_dev.bind_property(
            'hostname',
            self.hostname_label,
            'label',
            GObject.BindingFlags.SYNC_CREATE,
        )
        self.lan_dev.bind_property(
            'has_http',
            self.go_http_btn,
            'visible',
            GObject.BindingFlags.SYNC_CREATE,
        )
        self.lan_dev.bind_property(
            'has_ftp',
            self.go_ftp_btn,
            'visible',
            GObject.BindingFlags.SYNC_CREATE,
        )

    @Gtk.Template.Callback()
    def on_copy_btn_clicked(self, *_):
        clipboard_copy(self.lan_dev.address)

    @Gtk.Template.Callback()
    def on_go_http_btn_clicked(self, *_):
        Gio.AppInfo.launch_default_for_uri(f'http://{self.lan_dev.address}')

    @Gtk.Template.Callback()
    def on_go_ftp_btn_clicked(self, *_):
        bus = Gio.bus_get_sync(Gio.BusType.SESSION, None)
        file_manager = Gio.DBusProxy.new_sync(
            bus,
            Gio.DBusProxyFlags.NONE,
            None,
            'org.freedesktop.FileManager1',
            '/org/freedesktop/FileManager1',
            'org.freedesktop.FileManager1',
            None,
        )

        file_manager.call_sync(
            'ShowItems',
            GLib.Variant('(ass)', ([f'ftp://{self.lan_dev.address}'], '')),
            Gio.DBusCallFlags.NONE,
            -1,
            None,
        )
