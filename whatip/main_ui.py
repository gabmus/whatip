from gi.repository import Gtk, Adw, GObject, Gio
from whatip.lan_store import LanStore
from whatip.network_interface_store import NetworkInterfaceStore
from whatip.port_store import PortStore
from typing import cast


@Gtk.Template(resource_path='/org/gabmus/whatip/ui/main_ui.ui')
class MainUI(Gtk.Box):
    __gtype_name__ = 'MainUI'
    headerbar = cast(Gtk.HeaderBar, Gtk.Template.Child())
    squeezer = cast(Adw.Squeezer, Gtk.Template.Child())
    nobox = cast(Gtk.Label, Gtk.Template.Child())
    view_switcher = cast(Adw.ViewSwitcher, Gtk.Template.Child())
    bottom_bar = cast(Adw.ViewSwitcherBar, Gtk.Template.Child())
    refresh_btn = cast(Gtk.Button, Gtk.Template.Child())
    menu_btn = cast(Gtk.MenuButton, Gtk.Template.Child())
    main_stack = cast(Adw.ViewStack, Gtk.Template.Child())

    ip_page = cast(Adw.ViewStackPage, Gtk.Template.Child())
    ip_stack = cast(Gtk.Stack, Gtk.Template.Child())
    ip_empty = cast(Adw.StatusPage, Gtk.Template.Child())
    ip_view = cast(Gtk.ScrolledWindow, Gtk.Template.Child())
    ip_listbox = cast(Gtk.ListBox, Gtk.Template.Child())

    ports_page = cast(Adw.ViewStackPage, Gtk.Template.Child())
    ports_stack = cast(Gtk.Stack, Gtk.Template.Child())
    ports_empty = cast(Adw.StatusPage, Gtk.Template.Child())
    ports_view = cast(Gtk.ScrolledWindow, Gtk.Template.Child())
    ports_listbox = cast(Gtk.ListBox, Gtk.Template.Child())

    lan_page = cast(Adw.ViewStackPage, Gtk.Template.Child())
    lan_stack = cast(Gtk.Stack, Gtk.Template.Child())
    lan_empty = cast(Adw.StatusPage, Gtk.Template.Child())
    lan_view = cast(Gtk.Overlay, Gtk.Template.Child())
    lan_listbox = cast(Gtk.ListBox, Gtk.Template.Child())
    lan_progress_revealer = cast(Gtk.Revealer, Gtk.Template.Child())
    lan_progressbar = cast(Gtk.ProgressBar, Gtk.Template.Child())
    lan_scan_btn = cast(Gtk.Button, Gtk.Template.Child())
    lan_rescan_btn = cast(Gtk.Button, Gtk.Template.Child())

    def __init__(self, gsettings: Gio.Settings):
        super().__init__()
        self.gsettings = gsettings

        self.network_iterface_store = NetworkInterfaceStore(
            self.ip_listbox, self.gsettings
        )
        self.port_store = PortStore(self.ports_listbox)
        self.lan_store = LanStore(self.lan_listbox)

        self.network_iterface_store.connect(
            'items-changed',
            lambda *_: self.ip_stack.set_visible_child(
                self.ip_view
                if len(self.network_iterface_store) > 0
                else self.ip_empty
            ),
        )
        self.port_store.connect(
            'items-changed',
            lambda *_: self.ports_stack.set_visible_child(
                self.ports_view
                if len(self.port_store) > 0
                else self.ports_empty
            ),
        )

        self.lan_store.bind_property(
            'refreshing',
            self.lan_progress_revealer,
            'reveal-child',
            GObject.BindingFlags.DEFAULT,
        )

        self.lan_store.bind_property(
            'progress',
            self.lan_progressbar,
            'fraction',
            GObject.BindingFlags.DEFAULT,
        )

        for store in (
            self.network_iterface_store,
            self.port_store,
            self.lan_store,
        ):
            store.connect('notify::refreshing', self.on_refreshing_changed)

        self.on_squeeze()
        self.on_refreshing_changed()

    def on_refreshing_changed(self, *_):
        for btn in (self.refresh_btn, self.lan_scan_btn, self.lan_rescan_btn):
            btn.set_sensitive(
                not (
                    self.lan_store.refreshing
                    or self.network_iterface_store.refreshing
                )
            )

    @Gtk.Template.Callback()
    def on_squeeze(self, *_):
        self.bottom_bar.set_reveal(
            self.squeezer.get_visible_child() == self.nobox
        )

    @Gtk.Template.Callback()
    def on_refresh_btn_clicked(self, _):
        self.refresh_btn.set_sensitive(False)
        self.network_iterface_store.populate()
        self.port_store.populate()

    @Gtk.Template.Callback()
    def on_lan_scan_btn_clicked(self, _):
        if self.network_iterface_store.refreshing:
            return  # TODO: make lan scan button(s) insensitive instead
        self.lan_stack.set_visible_child(self.lan_view)
        self.lan_store.populate(self.network_iterface_store)
