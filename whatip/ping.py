from typing import Callable, Optional, Set
from gi.repository import Gio, GLib
from subprocess import Popen, DEVNULL, TimeoutExpired
from netaddr import IPNetwork, IPRange
from whatip.thread_pool import ThreadPool
import socket
from contextlib import closing
from gettext import gettext as _


MAX_THREADS = 30
COMMON_PORTS = [80, 21]


def run(cmd, stdout=DEVNULL, stderr=DEVNULL):
    p = Popen(cmd, shell=True, stdout=stdout, stderr=stderr)
    p.wait()
    try:
        out, err = p.communicate(timeout=5)
    except TimeoutExpired:
        p.kill()
        out, err = p.communicate()
    return (p.returncode, out, err)


def ports_scan(address: str) -> Set[int]:
    # Check ports HTTP, FTP
    open_ports = set()
    for port in COMMON_PORTS:
        with closing(
            socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        ) as sock:
            sock.setsockopt(  # Nagle's algorithm off
                socket.IPPROTO_TCP, socket.TCP_NODELAY, 1
            )
            sock.settimeout(0.5)  # host should repond in 500ms
            if sock.connect_ex((str(address), port)) == 0:
                open_ports.add(port)
    return open_ports


__bus = None
__avahiserver = None
try:
    __bus = Gio.bus_get_sync(Gio.BusType.SYSTEM, None)
    __avahiserver = Gio.DBusProxy.new_sync(
        __bus,
        Gio.DBusProxyFlags.NONE,
        None,
        'org.freedesktop.Avahi',
        '/',
        'org.freedesktop.Avahi.Server',
        None,
    )
except Exception:
    print(_('Error connecting to avahi daemon. Hostname resolution disabled.'))


def avahi_resolve_hostname(address: str) -> str:
    try:
        if __avahiserver is None or __bus is None:
            raise TypeError('avahiserver is None')
        return __avahiserver.call_sync(
            'ResolveAddress',
            GLib.Variant('(iisu)', (-1, -1, f'{address}', 0)),
            Gio.DBusCallFlags.NONE,
            -1,
            None,
        )[4]
    except Exception:
        return _('Unknown hostname')


def ping(address, cb: Callable[[Optional[dict]], None]):
    cmd = f'ping -c 1 -W1 {address}'
    valid = run(cmd)[0] == 0
    GLib.idle_add(
        cb,
        {
            'address': str(address),
            'hostname': avahi_resolve_hostname(address),
            'ports': ports_scan(address),
        } if valid else None,
    )


class NetScanner:
    def __init__(
        self,
        address: str,
        subnet_mask: int,
        cb: Callable[[Optional[dict]], None],
    ):
        self.address = address
        self.subnet_mask = subnet_mask
        ipa = IPNetwork(f'{address}/{subnet_mask}')
        self.ip_range = list(IPRange(ipa.first, ipa.last))
        self.pool = ThreadPool(
            MAX_THREADS,
            ping,
            [(addr, cb) for addr in self.ip_range],
        )

    def scan(self):
        self.pool.start()
