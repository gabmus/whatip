from gi.repository import Gdk


def clipboard_copy(txt):
    display = Gdk.Display.get_default()
    if display is not None:
        display.get_clipboard().set(txt)
